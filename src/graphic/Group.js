/* eslint-disable no-unused-vars */
import * as dataUtil from '../utils/data_structure_util';
import Element from './Element';
import Rect from './shape/Rect';

/**
 * @class qrenderer.graphic.Group
 *
 * - Group can have child nodes, not the other Element types.
 * - The transformations applied to Group will apply to its children too.
 *
 * - Group 可以插入子节点，其它类型不能。（仅仅是逻辑上限制，实际上根类 Element 带有 children 属性。）
 * - Group 上的变换也会被应用到子节点上。
 */
class Group extends Rect {
  /**
   * @method constructor Group
   */
  constructor(options = {}) {
    super(
      dataUtil.merge(
        {
          style: {
            fill: '#ccc',
          },
        },
        options,
        true,
      ),
    );

    /**
     * @property {String} type
     */
    this.type = 'group';
  }

  toJSONObject() {
    let result = Element.prototype.toJSONObject.call(this);
    result.linkable = this.linkable;
    return result;
  }
}

export default Group;
